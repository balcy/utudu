import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.ListItems 0.1 as ListItem
import "util.js" as Util
import Ubuntu.Components.Popups 0.1

Page {
    property var entryIndex: ""
    title: "Unset"

    onVisibleChanged: {
        if (visible === true) {
            updateCategories()
        }
    }

    function updateCategories() {
        categoriesModel.clear()

        var categories = storage.getCategories()
        for (var i = 0; i < categories.length; i++) {
            categoriesModel.append({ "name": categories[i] })
        }
    }

    ListModel {
        id: categoriesModel
    }

    DeleteDialog {
        id: categoryDeleteDialog
    }

    UbuntuShape {
        width: parent.width - units.gu(3)
        height: parent.height - units.gu(3)
        anchors.centerIn: parent
        radius: "small"
// FIXME: disable until dark color scheme bugs are worked out
//        color: storage.getThemeSetting("fgColor")

        Column {
            spacing: units.gu(1)
            anchors {
                margins: units.gu(2)
                fill: parent
            }

            Row {
                width: parent.width
                height: addCategoryTextField.height
                TextField {
                    id: addCategoryTextField
                    width: parent.width
                    placeholderText: i18n.tr("Add category...")

                    onAccepted: {
                        Util.debug("addCategoryTextField: entered '" +
                                   addCategoryTextField.text  + "'")
                        if (addCategoryTextField.text === "") {
                            // in 14.10, onAccepted is triggered when
                            // addCategoryTextField.text is set to empty string
                            // (which we do below)
                            return
                        } else if (storage.addCategory(addCategoryTextField.text) === true) {
                            addCategoryTextField.text = ""
                            updateCategories()
                        }
                    }
                }
            }

            ListView {
                id: categoriesList
                objectName: "categoriesList"
                width: parent.width
                height: parent.height - addCategoryTextField.height

                visible: true

                // make sure scrolling doesn't overlap other elements
                clip: true

                model: categoriesModel

                delegate: ListItem.Empty {
                    removable: true
                    highlightWhenPressed: false
                    confirmRemoval: true
                    showDivider: true
                    width: parent.width
                    height: categoryLabel.height + units.gu(3)

                    property var editing: false
                    property var cur: ListView.isCurrentItem

                    onPressAndHold: {
                        categoriesList.currentIndex = index
                        editing = true
                    }

                    onClicked: {
                        categoriesList.currentIndex = index
                    }

                    onCurChanged: {
                        // Don't allow concurrent edits
                        if (cur === false) {
                            editing = false
                        }
                    }

                    onEditingChanged: {
                        if (editing === true) {
                            // forget previous changes
                            editTextField.text = categoryLabel.text
                            editTextField.focus = true
                            editTextField.visible = true
                            categoryLabel.visible = false
                            editTextField.forceActiveFocus()
                            editTextField.cursorPosition = editTextField.text.length
                        } else {
                            editTextField.focus = false
                            editTextField.visible = false
                            categoryLabel.visible = true
                        }
                    }

                    Label {
                        id: categoryLabel
                        width: parent.width - units.gu(3)
// FIXME: disable until dark color scheme bugs are worked out
//                        color: storage.getThemeSetting("fgFontColor")
                        anchors.verticalCenter: parent.verticalCenter
                        text: index >= 0 ? categoriesModel.get(index)["name"] : ""
                        wrapMode: Text.WordWrap
                    }

                    TextField {
                        id: editTextField
                        anchors.verticalCenter: parent.verticalCenter
                        visible: false
                        text: categoryLabel.text
                        width: parent.width
                        height: parent.height - units.gu(1)

                        function processEditInput() {
                            Util.debug("editTextField input changed to '" +
                                       editTextField.text + "'")
                            var idx = storage.getCategoryIndex(categoriesModel.get(index)["name"])
                            if (storage.setCategoryName(idx,
                                editTextField.text) === true) {
                                categoryLabel.text = editTextField.text
                                updateCategories()
                            }
                            editing = false
                        }

                        onAccepted: {
                            processEditInput()
                        }
                        // Why isn't onAccepted working here?
                        Keys.onReturnPressed: {
                            processEditInput()
                        }
                    }

                    onItemRemoved: {
                        var idx = storage.getCategoryIndex(categoriesModel.get(index)["name"])
                        if (idx >= 0) {
                            storage.deleteCategory(idx)
                            updateCategories()
                        }
                    }
                }
            }
        } // Column
    } // UbuntuShape
}
