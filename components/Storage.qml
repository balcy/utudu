
/* Storage.qml
 *
 * Copyright 2014 Jamie Strandboge <jamie@ubuntu.com>
 * Copyright 2018 Chris Clime <chris.clime@gmx.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import QtQuick 2.0
import Qt.labs.settings 1.0
import "util.js" as Util

Item {
    id: storage

    // this string is here so any one can display it
    property var uncategorized: i18n.tr("Uncategorized")

    property var themes: {
        "Gray": {
            "headerColor": "#221e1c",
            "bgColor": "#221e1c",
            "footerColor": "#221e1c",
            "fgColor": "#333333",
            //"fgFontColor": "#aea79f",
            "fgFontColor": "#ffffff",
            "componentColor": "#333333",
            "componentFontColor": "#ffffff"
        },
        "Aubergine": {
            "headerColor": "#2c001e",
            "bgColor": "#5E2750",
            "footerColor": "#2c001e",
            "fgColor": "#2c001e",
            "fgFontColor": "#ffffff",
            "componentColor": "#2c001e",
            "componentFontColor": "#ffffff"
        },
        "Light": {
            "headerColor": "#545443",
            "bgColor": "#a8a887",
            "footerColor": "#a8a887",
            "fgColor": "#ffffcc",
            "fgFontColor": "#545443",
            "componentColor": "#787863",
            "componentFontColor": "#ffffff"
        }
    }

    //
    // Settings API
    //
    function getThemeSetting(attr) {
        var t = utuduSettings.theme
        return themes[t][attr]
    }

    function getTheme() {
        return utuduSettings.theme
    }

    function setTheme(theme) {
        utuduSettings.theme = theme
        return true
    }


    // Store this separately which will make it easier to do db updates in the
    // future. These might look like:
    //   if (db_version != db_version_latest) {
    //     settings.contents = tmp // we could be a lot smarter about this
    //                             // and just update the individual keys
    //   }
    //
    property var db_defaults: {
        "db_version": "1.0",
        "next_entry_index": "4",
        "next_category_index": "2",
        "entries": {
            "0": {
                "name": "Greetings from Utudu!",
                "type": "note",
                "modified": "1390759927000",
                "category": "-1", // Undefined/uncategorized
                "text": "Hello!\n\nUtudu is a simple checklist and note\ntaking program that is designed to be easy\nto use. It supports categories and two\ntypes of entries: checklists and notes.\n\nSide swipe an entry to delete and long\npress to edit.\n\nYou too can todo with Utudu!\n"
            },
            "1": {
                "name": "Groceries",
                "show_checked": "0",
                "type": "list",
                "modified": "1390760047000",
                "category": "0",
                "next_item_index": "2",
                "items": {
                    "0": {
                        "name": "milk",
                        "checked": "1",
                        "note": "always get 2%"
                    },
                    "1": {
                        "name": "eggs",
                        "checked": "0",
                        "note": ""
                    }
                }
            },
            "2": {
                "name": "Travel",
                "show_checked": "0",
                "type": "list",
                "modified": "1390759987000",
                "category": "1",
                "next_item_index": "3",
                "items": {
                    "0": {
                        "name": "toothbrush",
                        "checked": "0",
                        "note": ""
                    },
                    "1": {
                        "name": "toothpaste",
                        "checked": "0",
                        "note": ""
                    },
                    "2": {
                        "name": "hairbrush",
                        "checked": "0",
                        "note": ""
                    }
                }
            }
        },
        "categories": { // should not include "-1"
            "0": "Personal",
            "1": "Work"
        }
    }

   
    Settings {
        id: utuduSettings
        property string db_version: "1.0"
        property string theme: "Gray"
        property string items: JSON.stringify(storage.db_defaults)
   
        function saveItems() {
            this.items = JSON.stringify(internal.items)
        }
        
      }
      
     QtObject {
      id: internal
      property var items: JSON.parse(utuduSettings.items)
    }

    //
    // Items API
    //

    //
    // Category functions
    //

    // find index for a given category
    function getCategoryIndex(category) {
        if (category === uncategorized) {
            return -1
        }
        
        for (var idx in internal.items["categories"]) {
            if (internal.items["categories"][idx] === category) {
                return idx
            }
        }
        Util.error("getCategoryIndex(): category '" + category +
                   "' does not exist")

        return -1
    }

    // find category for a given index
    function getCategory(idx) {
        
        if (internal.items["categories"][idx] === undefined) {
            Util.error("getCategory(): idx '" + idx + "' does not exist")
            return -1
        }
        return internal.items["categories"][idx]
    }

    // return list of categories
    function getCategories() {
        
        var c = []
        for (var idx in internal.items["categories"]) {
            c.push(internal.items["categories"][idx])
        }
        c.sort()
        return c
    }

    function addCategory(category) {
        Util.debug("addCategory()")
        if (category === "-1") { // Undefined/uncategorized
            Util.error("addCategory(): may not use '-1' as category")
            return false
        } else if (category === "") {
            Util.error("addCategory(): may not use empty category")
            return false
        } else if (category === uncategorized) {
            Util.error("addCategory(): may not use '" + category +
                       "' as category")
            return false
        }

        var cat_idx = getCategoryIndex(category)
        if (cat_idx >= 0) {
            Util.error("addCategory(): category '" + category +
                       "'already exists")
            return false
        }

        var idx = internal.items["next_category_index"]

        internal.items["next_category_index"] = (idx * 1 + 1).toString()
        internal.items["categories"][idx] = category
        
        utuduSettings.saveItems()

        return true
    }

    function deleteCategory(idx) {
        Util.debug("deleteCategory()")
        var category = getCategory(idx)
        if (category < 0) {
            return false
        }

        // Adjust category for any entries with this category to be
        // uncategorized
        var entries = getEntryIndexesByCategory(category)
        for (var i = 0; i < entries.length; i++) {
            internal.items["entries"][entries[i]]["category"] = "-1"
        }

        delete internal.items["categories"][idx]
        
        utuduSettings.saveItems()
        
        return true
    }

    function setCategoryName(idx, text) {
        Util.debug("setCategoryName('" + idx + ", '" + text + "')")
        if (internal.items["categories"][idx] === undefined) {
            return false
        }

        internal.items["categories"][idx] = text
        
        utuduSettings.saveItems()
                
        return true
    }
    //
    // entry items functions
    //

    function getNoteTextByEntry(idx) {
        
        Util.debug("getNoteTextByEntry(" + idx + ")")
        if (idx === "") {
            return ""
        }
        if (internal.items["entries"][idx] === undefined) {
            return ""
        }
        if (internal.items["entries"][idx]["type"] !== "note") {
            Util.error("getNoteTextEntry(): entry idx '" + idx + "' not a note")
            return ""
        }
        return internal.items["entries"][idx]["text"]
    }

    function setNoteTextByEntry(idx, text) {
        Util.debug("setNoteTextByEntry(" + idx + ")")
        if (idx === "") {
            Util.error("setNoteTextByEntry(): idx is empty")
            return false
        }
        if (internal.items["entries"][idx] === undefined) {
            Util.error("setNoteTextByEntry(): idx '" + idx + "' does not exist")
            return false
        }
        if (internal.items["entries"][idx]["type"] !== "note") {
            Util.error("setNoteTextByEntry(): entry idx '" + idx + "' not a note")
            return false
        }

        internal.items["entries"][idx]["text"] = text
        
        utuduSettings.saveItems()

        return true
    }

    function getItemsByEntry(idx) {
        
        Util.debug("getItemsByEntry(" + idx + ")")
        var items_arr = []

        if (idx === "") {
            return items_arr
        }
        if (internal.items["entries"][idx] === undefined) {
            return items_arr
        }
        if (internal.items["entries"][idx]["type"] !== "list") {
            Util.error("getItemsByEntry(): entry idx '" + idx + "' not a list")
            return items_arr
        }

        // return array of items sorted by index
        var indexes = Object.keys(internal.items["entries"][idx]["items"])
        indexes.sort(function(a,b){return a-b})
        for (var i = 0; i < indexes.length; i++) {
            var item_idx = indexes[i]
            var item = internal.items["entries"][idx]["items"][item_idx]
            // FIXME: add this so the ListModel has it
            item["listModelIndex"] = item_idx
            items_arr.push(item)
        }

        return items_arr
    }

    function addListItem(idx, s) {
        Util.debug("addListItem(" + idx + ", " + s + ", " + internal.items["entries"][idx]["next_item_index"] + ")")
        if (internal.items["entries"][idx] === undefined) {
            Util.error("addListItem(): idx '" + idx + "' does not exist")
            return null
        }

        var next_idx = internal.items["entries"][idx]["next_item_index"]

        internal.items["entries"][idx]["items"][next_idx] = {}
        internal.items["entries"][idx]["items"][next_idx]["name"] = s
        internal.items["entries"][idx]["items"][next_idx]["checked"] = "0"
        internal.items["entries"][idx]["items"][next_idx]["note"] = ""
        internal.items["entries"][idx]["next_item_index"] = (next_idx * 1 + 1).toString()

        // FIXME: add this so the ListModel has it
        internal.items["entries"][idx]["items"][next_idx]["listModelIndex"] = next_idx
        
        utuduSettings.saveItems()
        
        return internal.items["entries"][idx]["items"][next_idx]
    }

    function deleteListItem(idx, item_idx) {
        Util.debug("deleteListItem(" + idx + ", " + item_idx + ")")
        if (internal.items["entries"][idx] === undefined) {
            Util.error("deleteListItem(): idx '" + idx + "' does not exist")
            return false
        }
        if (internal.items["entries"][idx]["items"][item_idx] === undefined) {
            Util.error("deleteListItem(): item_idx '" + item_idx + "' does not exist")
            return false
        }

        delete internal.items["entries"][idx]["items"][item_idx]
        
        utuduSettings.saveItems()
        
        return true
    }

    function setListItemChecked(entry_idx, item_idx, checked) {
        Util.debug("setListItemChecked(" + entry_idx + ", " + item_idx +
                   ", " + checked + ")")
        if (internal.items["entries"][entry_idx] === undefined || entry_idx === "") {
            Util.error("setListItemChecked(): entry_idx '" + entry_idx +
                       "' does not exist")
            return -1
        }
        if (internal.items["entries"][entry_idx]["items"][item_idx] === undefined) {
            Util.error("setListItemChecked(): item '" + item_idx +
                       "' does not exist")
            return -1
        }

        internal.items["entries"][entry_idx]["items"][item_idx]["checked"] = checked
        
        utuduSettings.saveItems()
        
        return true
    }

    function setListItemName(entry_idx, item_idx, text) {
        Util.debug("setListItemName(" + entry_idx + ", " + item_idx +
                   ", " + text + ")")
        if (internal.items["entries"][entry_idx] === undefined || entry_idx === "") {
            Util.error("setListItemName(): entry_idx '" + entry_idx +
                       "' does not exist")
            return false
        }
        if (internal.items["entries"][entry_idx]["items"][item_idx] === undefined) {
            Util.error("setListItemName(): item '" + item_idx +
                       "' does not exist")
            return false
        }

        internal.items["entries"][entry_idx]["items"][item_idx]["name"] = text
        
        utuduSettings.saveItems()
        
        return true
    }

    //
    // entries functions
    //

    // category should be category name or storage.uncategorized for all
    function getEntryIndexesByCategory(category) {
        
        var e = []
        var cat_idx = null

        // if category specified, then find the index, if it can't be found,
        // return empty list
        if (category === "-1") { // Using "-1" explicitly is an error
            return e
        } else if (category !== uncategorized && category !== "") {
            cat_idx = getCategoryIndex(category)
            if (cat_idx < 0) {
                return e
            }
        }

        // return array of items sorted by index
        var indexes = Object.keys(internal.items["entries"])
        indexes.sort(function(a,b){return a-b})
        for (var i = 0; i < indexes.length; i++) {
            var idx = indexes[i]
            // cat_idx is null when category is storage.uncategorized or ""
            if (cat_idx === null ||
                internal.items["entries"][idx]["category"] === cat_idx) {
                e.push(idx)
            }
        }
        return e
    }

    // Return list of entries by categories
    function getEntriesByCategory(category) {
        
        var entries_arr = []
        var cat_idx = null

        // if category specified, then find the index, if it can't be found,
        // return
        if (category !== uncategorized && category !== "") {
            cat_idx = getCategoryIndex(category)
            if (cat_idx < 0) {
                return entries_arr
            }
        }

        // return array of items sorted by index
        var indexes = Object.keys(internal.items["entries"])
        indexes.sort(function(a,b){return a-b})
        for (var i = 0; i < indexes.length; i++) {
            var idx = indexes[i]
            // cat_idx is null when category is storage.uncategorized
            if (cat_idx === null ||
                internal.items["entries"][idx]["category"] === cat_idx) {
                var entry = internal.items["entries"][idx]
                // FIXME: add this so the ListModel has it
                entry["listModelIndex"] = idx
                entries_arr.push(entry)
            }
        }

        return entries_arr
    }

    function getEntryName(idx) {
        
        Util.debug("getEntryName(" + idx + ")")
        if (internal.items["entries"][idx] === undefined) {
            return ""
        }
        return internal.items["entries"][idx]["name"]
    }

    function setEntryName(idx, text) {
        Util.debug("setEntryName(" + idx + ", '" + text + "')")
        if (internal.items["entries"][idx] === undefined) {
            return false
        }
        
        internal.items["entries"][idx]["name"] = text
        
        utuduSettings.saveItems()
        
        return true
    }

    function addEntry(entry, category, type) {
        Util.debug("addEntry()")
        var cat_idx = getCategoryIndex(category)
        if (type !== "list" && type !== "note") {
            Util.error("type '" + type + "' is not 'list' or 'note'")
            return false
        }

        var idx = internal.items["next_entry_index"]
   
        internal.items["next_entry_index"] = (idx * 1 + 1).toString()
        internal.items["entries"][idx] = {}
        internal.items["entries"][idx]["type"] = type
        internal.items["entries"][idx]["name"] = entry
        if (category === uncategorized || cat_idx < 0) {
            internal.items["entries"][idx]["category"] = "-1" // Undefined
        } else {
            internal.items["entries"][idx]["category"] = cat_idx.toString()
        }
        // modified is milliseconds since epoch (divide by 1000 to get epoch)
        internal.items["entries"][idx]["modified"] = (new Date().getTime()).toString()

        if (type === "list") {
            internal.items["entries"][idx]["show_checked"] = "0"
            internal.items["entries"][idx]["items"] = {}
            internal.items["entries"][idx]["next_item_index"] = "0"

        } else {
            internal.items["entries"][idx]["text"] = ""
        }
        
        utuduSettings.saveItems()
            
        return idx
    }


    function deleteEntry(idx) {
        Util.debug("deleteEntry(" + idx + ")")
        if (internal.items["entries"][idx] === undefined) {
            Util.error("deleteEntry(): idx '" + idx + "' does not exist")
            return false
        }

        delete internal.items["entries"][idx]

        utuduSettings.saveItems()
        
        return true
    }

    function getEntryCategory(idx) {
        
        Util.debug("getEntryCategory(" + idx + ")")
        if (internal.items["entries"][idx] === undefined) {
            Util.error("getEntryCategory(): idx '" + idx + "' does not exist")
            return -1
        }
        var cat = internal.items["entries"][idx]["category"]
        if (cat === "-1") {
            cat = uncategorized
        }
        return cat
    }

    function setEntryCategory(idx, category) {
        Util.debug("setEntryCategory(" + idx + ", '" + category + "')")
        if (internal.items["entries"][idx] === undefined) {
            return false
        }
        var cat_idx = getCategoryIndex(category)

        internal.items["entries"][idx]["category"] = cat_idx
        
        utuduSettings.saveItems()
        
        return true
    }

    function getEntryShowChecked(idx) {
        
        Util.debug("getEntryShowChecked(" + idx + ")")
        if (internal.items["entries"][idx] === undefined || idx === "") {
            Util.error("getEntryShowChecked(): idx '" + idx + "' does not exist")
            return -1
        }
        if (internal.items["entries"][idx]["show_checked"] === "1") {
            return true
        }
        return false
    }

    function setEntryShowChecked(idx, show_checked) {
        Util.debug("setEntryShowChecked(" + idx + ", " + show_checked + ")")
        if (internal.items["entries"][idx] === undefined) {
            Util.error("getEntryShowChecked(): idx '" + idx + "' does not exist")
            return false
        }

        internal.items["entries"][idx]["show_checked"] = show_checked
        
        utuduSettings.saveItems()
        
        return true
    }

    function getEntryCategoryName(idx) {
        
        Util.debug("getEntryCategoryName(" + idx + ")")
        var cat_idx = getEntryCategory(idx)
        if (idx < 0) {
            return -1
        }
        return getCategory(cat_idx)
    }

    function isEntryList(idx) {
        
        Util.debug("isEntryList(" + idx + ")")
        if (internal.items["entries"][idx] === undefined) {
            Util.error("isEntryList(): idx '" + idx + "' does not exist")
            return false
        }

        if (internal.items["entries"][idx]["type"] === "list") {
            return true
        }
        return false
    }
}
