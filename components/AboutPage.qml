import QtQuick 2.0
import Ubuntu.Components 0.1
import "util.js" as Util

Page {
    id: aboutPage
    visible: false
    property var version: "0.3"
    Column {
        id: aboutColumn
        spacing: units.gu(3)
        width: parent.width
        y: units.gu(6)

        Label {
            fontSize: "x-large"
            font.bold: true
            text: "Utudu"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Label {
            font.bold: true
            text: "(yoo-too-DOO)"
            anchors.horizontalCenter: parent.horizontalCenter
        }

        // Utilize image with transparency
        UbuntuShape {
            width: 256
            height: 240 // don't show transparency
            anchors.horizontalCenter: parent.horizontalCenter

            image: Image {
                fillMode: Image.PreserveAspectCrop
                source: "../graphics/utudu256.png"
                verticalAlignment: Image.AlignVCenter
                horizontalAlignment: Image.AlignHCenter
                sourceSize.width: parent.width
                sourceSize.height: parent.height
            }
        }
        Grid {
            anchors.horizontalCenter: parent.horizontalCenter
            columns: 2
            spacing: units.gu(1)
            Label {
                id: "versionLabel"
                font.bold: true
                text: i18n.tr("Version: ")
            }
            Label {
                text: aboutPage.version
            }
            Label {
                font.bold: true
                text: i18n.tr("Author: ")
            }
            Label {
                text: "Jamie Strandboge"
            }
            Label {
                font.bold: true
                text: i18n.tr("Icon: ")
            }
            Label {
                text: "Sam Hewitt"
            }
            Label {
                font.bold: true
                text: i18n.tr("Licenses: ")
            }
            Label {
                text: "<a href=\"http://www.gnu.org/licenses/gpl-3.0.html#content\">GPL-3</a>"
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            Label {
                font.bold: true
                text: "<a href=\"https://launchpad.net/utudu\">https://launchpad.net/utudu</a>"
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
/* TODO: needs flickable
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            Text {
                color: versionLabel.color
                font.pointSize: versionLabel.font.pointSize
                font.family: versionLabel.font.family
                text: "<b>Notes:</b><br>
<br>= 0.2.1 =</br>
* theming temporarily disabled until SDK bugs are fixed<br>
* may want to disable auto-punctuation until SDK exposes it<br>
"
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
*/
    }
}
