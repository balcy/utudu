import QtQuick 2.0
import Ubuntu.Components 0.1
import "util.js" as Util
import Ubuntu.Components.Themes.Ambiance 0.1

Page {
    title: "New entry"
    // TODO: keep these in sync with entriesPage (but can't now since setting
    // selectedIndex doesn't work right
    property var category: storage.uncategorized
    property var addCategoryText: i18n.tr("Add category...")
    property var categories: [storage.uncategorized].concat(storage.getCategories())

    UbuntuShape {
        width: parent.width - units.gu(3)
        height: parent.height - units.gu(3)
        anchors.centerIn: parent
        radius: "small"
// FIXME: disable until dark color scheme bugs are worked out
//        color: storage.getThemeSetting("fgColor")

        Column {
            spacing: units.gu(1)
            anchors {
                margins: units.gu(2)
                fill: parent
            }

            OptionSelector {
                id: categorySelector
                model: categories
                selectedIndex: categories.indexOf(category)

                onDelegateClicked: {
                    Util.debug("NewEntryPage: selected category")
                    category = categories[index]
                }
                // This should not work. I don't understand why it does. I
                // can't seem to change the font color though. If it breaks,
                // then disable the Light theme
                style: TextAreaStyle {
                    background: UbuntuShape {
                        radius: "medium"
// FIXME: disable until dark color scheme bugs are worked out
//                        color: storage.getThemeSetting("componentColor")
                    }
                }
            }

            OptionSelector {
                id: typeSelector
                width: parent.width
                property var values: [i18n.tr("Checklist"), i18n.tr("Note")]
                selectedIndex: 0
                model: values

                function getIndexOfOption(type) {
                    if (type === "list") {
                        return 0
                    }
                    return 1
                }
                // This should not work. I don't understand why it does. I
                // can't seem to change the font color though. If it breaks,
                // then disable the Light theme
                style: TextAreaStyle {
                    background: UbuntuShape {
                        radius: "medium"
// FIXME: disable until dark color scheme bugs are worked out
//                        color: storage.getThemeSetting("componentColor")
                    }
                }
            }

            Row {
                width: parent.width
                height: addEntryTextField.height
                TextField {
                    id: addEntryTextField
                    placeholderText: i18n.tr("Add new entry...")
                    width: parent.width
                    //textColor: storage.getThemeSetting("fgFontColor")

                    onAccepted: {
                        Util.debug("addEntryTextField: entered '" +
                                   addEntryTextField.text  + "'")
                        if (addEntryTextField.text === "") {
                            return
                        }

                        var type = "note"
                        var view = noteView
                        if (typeSelector.selectedIndex ===
                            typeSelector.getIndexOfOption("list")) {
                            type = "list"
                            view = listView
                        }
                        var idx = storage.addEntry(addEntryTextField.text,
                                  category, type)
                        if (idx >= 0) {
                            entriesPage.updateEntries()
                            view.title = storage.getEntryName(idx)
                            view.entryIndex = idx

                            pageStack.pop()
                            pageStack.push(view)
                            addEntryTextField.text = ""
                        }
                    }
                }
            }
        } // Column
    } // UbuntuShape
}
