import QtQuick 2.0
import Ubuntu.Components 0.1
import "util.js" as Util

Page {
    Column {
        spacing: units.gu(1)
        anchors {
            margins: units.gu(2)
            fill: parent
        }
        width: parent.width

        OptionSelector {
            id: themeSelector
            text: i18n.tr("Theme:")
            property var values: ["Gray", "Aubergine"] // TODO: don't hardcode
            //property var values: ["Gray", "Aubergine", "Light (experimental)"] // TODO: don't hardcode
            selectedIndex: values.indexOf(storage.getTheme())
            model: values

            onDelegateClicked: {
                Util.debug("settingsPage: chose " + values[index])
                var t = values[index]
                if (t === "Light (experimental)") {
                    t = "Light"
                }
                storage.setTheme(t)
                mainView.headerColor = storage.getThemeSetting("headerColor")
                mainView.backgroundColor = storage.getThemeSetting("bgColor")
                mainView.footerColor = storage.getThemeSetting("footerColor")
            }
        }
    }
}
