import QtQuick 2.0
import Ubuntu.Components 0.1
import "util.js" as Util
import Ubuntu.Components.Themes.Ambiance 0.1

Page {
    title: i18n.tr("Change category")
    property var category: storage.uncategorized
    property var addCategoryText: i18n.tr("Change category")
    property var categories: [storage.uncategorized].concat(storage.getCategories())
    property var entryIndex: ""

    onEntryIndexChanged: {
        Util.debug("ChangeCategoryPage: intryIndex = " + entryIndex)
        category = storage.getEntryCategoryName(entryIndex)
        categorySelector.selectedIndex = categories.indexOf(category)
    }

    UbuntuShape {
        width: parent.width - units.gu(3)
        height: parent.height - units.gu(3)
        anchors.centerIn: parent
        radius: "small"
// FIXME: disable until dark color scheme bugs are worked out
//        color: storage.getThemeSetting("fgColor")

        Column {
            spacing: units.gu(1)
            anchors {
                margins: units.gu(2)
                fill: parent
            }

            Label {
                text: i18n.tr("Adjust category to:")
            }

            OptionSelector {
                id: categorySelector
                model: categories
                selectedIndex: categories.indexOf(category)

                onDelegateClicked: {
                    Util.debug("ChangeCategoryPage: selected category")
                    category = categories[index]
                    if (storage.setEntryCategory(entryIndex, category) === true) {
                        pageStack.pop()
                    }
                }
                // This should not work. I don't understand why it does. I
                // can't seem to change the font color though. If it breaks,
                // then disable the Light theme
                style: TextAreaStyle {
                    background: UbuntuShape {
                        radius: "medium"
// FIXME: disable until dark color scheme bugs are worked out
//                        color: storage.getThemeSetting("componentColor")
                    }
                }
            }
        } // Column
    } // UbuntuShape
}
