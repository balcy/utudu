import QtQuick 2.0
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 0.1 as ListItem
import "util.js" as Util
import Ubuntu.Components.Themes.Ambiance 0.1

Page {
    title: i18n.tr("Utudu")
    property var numEntries: -1
    property var showAllText: i18n.tr("All")

    // TODO: keep these in sync with entriesPage (but can't now since setting
    // selectedIndex doesn't work right
    property var filterCategory: ""
    property var filterCategoryIndex: 0

    onNumEntriesChanged: {
        entriesModel.clear()

        var s = (filterCategory !== showAllText ? filterCategory : "")
        var entries = storage.getEntriesByCategory(s)

        // sorted by when entered (index)
        //for (var i = 0; i < entries.length; i++) {
        //    entriesModel.append(entries[i])
        //}

        // sorted alphabetical by name
        var names = {}
        for (var i = 0; i < entries.length; i++) {
            var n = entries[i]["name"].toLowerCase()
            if (names.hasOwnProperty(n)) {
                names[n].push(entries[i])
            } else {
                names[n] = []
                names[n].push(entries[i])
            }
        }
        var keys = []
        for (var k in names) {
            keys.push(k)
        }
        keys.sort()
        for (var i = 0; i < keys.length; i++) {
            for (var j = 0; j < names[keys[i]].length; j++) {
                entriesModel.append(names[keys[i]][j])
            }
        }
    }

    onVisibleChanged: {
        if (visible === true) {
            // This just resets to 'All', which is not ideal
            // filterCategory = showAllText

            // This works ok, but see the optionSelector change and it doesn't
            // handle when the selected category's name changes
            if (filterCategoryIndex < filterSelector.values.length &&
                filterSelector.values[filterCategoryIndex] === filterCategory) {
                filterSelector.selectedIndex = filterCategoryIndex
            } else {
                filterCategory = showAllText
            }
        }
    }

    onFilterCategoryChanged: {
        if (filterCategory === undefined) {
            return
        }
        Util.debug("EntriesPage: filterCategory changed to '" +
                   filterCategory + "'")
        updateEntries()
    }

    Component.onCompleted: {
        filterCategory = showAllText
    }

    function updateEntries() {
        var s = (filterCategory !== showAllText ? filterCategory : "")
        numEntries = storage.getEntryIndexesByCategory(s).length
    }

    tools: ToolbarItems {

        ToolbarButton {
            id: newEntryButton
            objectName: "newEntryButton"
            action: Action {
                text: i18n.tr("New")
                iconSource: Util.getIcon("add")
                onTriggered: {
                    Util.debug("pressed newEntryButton")
                    pageStack.push(newEntry)
                }
            }
        }
        ToolbarButton {
            id: editCategoriesButton
            objectName: "editCategoriesButton"
            action: Action {
                text: i18n.tr("Categories")
                iconSource: Util.getIcon("edit")
                onTriggered: {
                    Util.debug("pressed editCategoriesButton")
                    editCategories.title = i18n.tr("Edit Categories")
                    editCategories.entryIndex = ""
                    pageStack.push(editCategories)
                }
            }
        }
        /* Just display in the HUD
        ToolbarButton {
            id: aboutButton
            objectName: "aboutButton"
            action: Action {
                text: i18n.tr("About")
                iconSource: Util.getIcon("help")
                onTriggered: {
                    Util.debug("pressed newEntryButton")
                    pageStack.push(aboutPage)
                }
            }
        }
        */

        /* TODO
        ToolbarButton {
            id: searchButton
            objectName: "searchButton"
            action: Action {
                text: i18n.tr("Search")
                iconSource: Util.getIcon("search")
                onTriggered: {
                    Util.debug("pressed searchButton")
                }
            }
        }
        */

        /* Just display in the HUD
        ToolbarButton {
            id: settingsButton
            objectName: "settingsButton"
            action: Action {
                text: i18n.tr("Settings")
                iconSource: Util.getIcon("settings")
                onTriggered: {
                    pageStack.push(settingsPage)
                }
            }
        }
        */
    }

    ListModel {
        id: entriesModel
    }

    UbuntuShape {
        width: parent.width - units.gu(3)
        height: parent.height - units.gu(3)
        anchors.centerIn: parent
        radius: "small"
// FIXME: disable until dark color scheme bugs are worked out
//        color: storage.getThemeSetting("fgColor")

        Column {
            spacing: units.gu(1)
            anchors {
                margins: units.gu(2)
                fill: parent
            }
            width: parent.width

/*
            ListItem.ValueSelector {
                id: filterSelector
                values: [showAllText].concat(storage.getCategories())
                text: i18n.tr("Filter by category:")

                onSelectedIndexChanged: {
                    Util.debug("EntriesPage: selected category")
                    filterCategory = values[selectedIndex]
                    filterCategoryIndex = selectedIndex
                }
            }
*/
            OptionSelector {
                id: filterSelector
                property var values: [showAllText].concat(storage.getCategories())
                model: values
                highlightWhenPressed: false

                onDelegateClicked: {
                    Util.debug("EntriesPage: selected category")
                    filterCategory = values[index]
                    filterCategoryIndex = index
                }

/*
                // This should not work. I don't understand why it does. I
                // can't seem to change the font color though. If it breaks,
                // then disable the Light theme
                style: TextAreaStyle {
                    background: UbuntuShape {
                        radius: "medium"
// FIXME: disable until dark color scheme bugs are worked out
//                        color: storage.getThemeSetting("componentColor")
                    }
                }

// TODO: when theming is working right, perhaps do this
                style: OptionSelectorStyle {
                    UbuntuShape {
                        width: styledItem.width
                        height: styledItem.height
                        radius: "medium"
                        color: storage.getThemeSetting("componentColor")
                    }
                }
*/

            }

            ListView {
                id: entriesList
                width: parent.width
                height: parent.height - filterSelector.height
                visible: true

                // make sure scrolling doesn't overlap other elements
                clip: true

                // Scroll to editing item quickly
                highlightMoveVelocity: 2500

                model: entriesModel

                delegate: ListItem.Empty {
                    highlightWhenPressed: false
                    removable: true
                    confirmRemoval: true

                    showDivider: true
                    height: entryLabel.height + units.gu(3)

                    onItemRemoved: {
                        storage.deleteEntry(entriesModel.get(index)["listModelIndex"])
                        updateEntries()
                    }

                    property var editing: false
                    property var cur: ListView.isCurrentItem

                    onPressAndHold: {
                        entriesList.currentIndex = index
                        editing = true
                    }

                    onClicked: {
                        entriesList.currentIndex = index

                        var idx = entriesModel.get(index)["listModelIndex"]
                        var view = noteView
                        if (storage.isEntryList(idx) === true) {
                            view = listView
                        }
                        view.title = entriesModel.get(index)["name"]
                        view.entryIndex = idx
                        pageStack.push(view)
                    }

                    onCurChanged: {
                        // Don't allow concurrent edits
                        if (cur === false) {
                            editing = false
                        }
                    }

                    onEditingChanged: {
                        if (editing === true) {
                            // forget previous changes
                            editTextField.text = entryLabel.text
                            editTextField.focus = true
                            editTextField.visible = true
                            entryLabel.visible = false
                            editTextField.forceActiveFocus()
                            editTextField.cursorPosition = editTextField.text.length
                        } else {
                            editTextField.focus = false
                            editTextField.visible = false
                            entryLabel.visible = true
                        }
                    }

                    Label {
                        id: entryLabel
                        height: contentHeight + units.gu(0.5)
                        width: parent.width - units.gu(3)
// FIXME: disable until dark color scheme bugs are worked out
//                        color: storage.getThemeSetting("fgFontColor")
                        wrapMode: Text.WordWrap
                        text: index >= 0 ? entriesModel.get(index)["name"] : ""
                        anchors.verticalCenter: parent.verticalCenter
                    }

                    TextArea {
                        id: editTextField
                        anchors.verticalCenter: parent.verticalCenter
                        visible: false
                        text: entryLabel.text
                        width: parent.width
                        height: units.gu(4.3)
                        autoSize: true

                        function processEditInput() {
                            Util.debug("editTextField input changed to '" +
                                       editTextField.text + "'")
                            var idx = entriesModel.get(index)["listModelIndex"]
                            if (storage.setEntryName(idx,
                                editTextField.text) === true) {
                                entryLabel.text = editTextField.text
                                updateEntries()
                            }
                            editing = false
                        }

                        // Why isn't onAccepted working here?
                        Keys.onReturnPressed: {
                            processEditInput()
                        }
                    }
                }
            }
        } // Column
    } // UbuntuShape
}
