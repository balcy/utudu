import QtQuick 2.0
import Ubuntu.Components 1.1
import Ubuntu.Components.ListItems 0.1 as ListItem
import "components"
import "components/util.js" as Util

/*!
    \brief MainView with a Label and Button elements.
*/

MainView {
    // objectName for functional testing purposes (autopilot-qt5)
    objectName: "mainView"
    id: mainView

    // Note! applicationName needs to match the "name" field of the click manifest
    applicationName: "openstore.utudu"

    /*
     This property enables the application to change orientation
     when the device is rotated. The default is false.
    */
    automaticOrientation: true

    // We haven't transitioned yet
    useDeprecatedToolbar: false

    // Resize contents when keyboard appears
    anchorToKeyboard: true

    width: units.gu(50)
    height: units.gu(75)

// FIXME: disable until dark color scheme bugs are worked out
//    headerColor: storage.getThemeSetting("headerColor")
//    backgroundColor: storage.getThemeSetting("bgColor")
//    footerColor: storage.getThemeSetting("footerColor")

// TODO: use this when LP: #1389792 is fixed
//    Component.onCompleted: {
//        Theme.name = 'themes.Utudu'
//    }

    // http://developer.ubuntu.com/api/devel/ubuntu-13.10/c/unity-action/platform    -integration.html
     actions: [
         Action {
             text: "Settings"
             keywords: "Settings"
             onTriggered: {
                 pageStack.push(settingsPage)
             }
         },
         Action {
             text: "About"
             keywords: "About"
             onTriggered: {
                 pageStack.push(aboutPage)
             }
         }
    ]

    Storage {
        id: myStorage
    }
    property alias storage: myStorage

    PageStack {
        id: pageStack
        anchors.fill: parent
        Component.onCompleted: {
            Util.debug("Starting...")
            pageStack.push(entriesPage)
        }

        EntriesPage {
            id: entriesPage
            visible: true
        }

        NewEntryPage {
            id: newEntry
            visible: false
        }

        EditCategoriesPage {
            id: editCategories
            visible: false
        }

        ChangeCategoryPage {
            id: changeCategory
            visible: false
        }

        ItemsPage {
            id: listView
            visible: false
        }

        NotePage {
            id: noteView
            visible: false
        }

        SettingsPage {
            id: settingsPage
            visible: false
        }

        AboutPage {
            id: aboutPage
            visible: false
        }
    }
}
