#!/bin/bash

debug=false
[[ "$1" == "debug" ]] && debug=true

cd ./tmp

echo "Adjust the version"
version=`grep 'property var version: ' ./components/AboutPage.qml | cut -d ':' -f 2 | cut -d '"' -f 2`
sed -i "s/@VERSION@/$version/" ./manifest.json

echo "Adjust debugging output"
sed -i "s/var debugging = true/var debugging = $debug/" ./components/util.js
